/*
 * context.c
 *
 * Copyright (c) 2017 endaaman
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */
#include "internal.h"
#include <gtk/gtk.h>
#include <vte/vte.h>

/*
 * context
 */
typedef struct
{
    bool has_custom;
    GSList* custom_key_pairs;
} Keymap;

struct context_t
{
    view_t	    view;
    VteTerminal     *vte;
    void	    *extra;
    config_t	    config;
    Keymap	    *keymap;
    bool	    loading;
};

vterm_t context_vterm (context_t c) {return (vterm_t) c->vte;}
view_t	context_view  (context_t c) {return c->view;}
void *	context_extra (context_t c) {return c->extra;}

typedef struct context_t Context;

/*
 * commands
 */
static void command_reload (Context* context)
{
    context_load (context);
}

static void command_copy_clipboard (Context* context)
{
    vte_terminal_copy_clipboard_format(context->vte, VTE_FORMAT_TEXT);
}

static void command_paste_clipboard (Context* context)
{
    vte_terminal_paste_clipboard (context->vte);
}

static void command_increase_font_scale(Context* context)
{
    VteTerminal* vte = context->vte;
    double scale = vte_terminal_get_font_scale(vte) + 0.1;
    vte_terminal_set_font_scale(vte, scale);
}

static void command_decrease_font_scale (Context* context)
{
    VteTerminal* vte = context->vte;
    double scale = vte_terminal_get_font_scale(vte) - 0.1;
    vte_terminal_set_font_scale(vte, scale);
}

static void command_reset_font_scale (Context* context)
{
    vte_terminal_set_font_scale(context->vte, 1.0);
}

/*
 * keymaps
 */
typedef struct
{
    unsigned key;
    GdkModifierType mod;
    char* func_key;
} CustomKeyPair;

static void custom_key_pair_free(CustomKeyPair* pair)
{
    g_free(pair->func_key);
    g_free(pair);
}

static void keymap_reset (Keymap* keymap)
{
    g_slist_foreach (keymap->custom_key_pairs, (GFunc)custom_key_pair_free, NULL);
    g_slist_free (keymap->custom_key_pairs);
    keymap->custom_key_pairs = NULL;
    keymap->has_custom	     = false;
}

static Keymap* keymap_init (void)
{
    Keymap* keymap = g_malloc0(sizeof(Keymap));
    //keymap->lua = lua;
    keymap->custom_key_pairs = NULL;
    keymap->has_custom = false;
    keymap_reset(keymap);
    return keymap;
}

static void keymap_close(Keymap* keymap)
{
    keymap_reset(keymap);
    g_free(keymap);
}

/*
 * aplly config
 */
#define CURSOR_BLINK_MODE_SYSTEM "system"
#define CURSOR_BLINK_MODE_ON "on"
#define CURSOR_BLINK_MODE_OFF "off"
#define CJK_WIDTH_NARROW "narrow"
#define CJK_WIDTH_WIDE "wide"

typedef enum {
  VTE_CJK_WIDTH_NARROW = 1,
  VTE_CJK_WIDTH_WIDE = 2
} VteCjkWidth;

static VteCursorBlinkMode match_cursor_blink_mode (const char* str)
{
    if (0 == g_strcmp0(str, CURSOR_BLINK_MODE_ON)) {
	return VTE_CURSOR_BLINK_ON;
    }
    if (0 == g_strcmp0(str, CURSOR_BLINK_MODE_OFF)) {
	return VTE_CURSOR_BLINK_OFF;
    }
    return VTE_CURSOR_BLINK_SYSTEM;
}

static unsigned match_cjk_width(const char* str)
{
    if (0 == g_strcmp0(str, CJK_WIDTH_WIDE)) {
	return VTE_CJK_WIDTH_WIDE;
    }
    return VTE_CJK_WIDTH_NARROW;
}

typedef void (*VteSetColorFunc)(VteTerminal*, const GdkRGBA*);

static void config_apply_color (config_t c, VteTerminal* vte, VteSetColorFunc vte_set_color_func, const char* key)
{
    const char *val = config_get_string (c, key, NULL);
    if (!val) return;

    GdkRGBA color;
    bool valid = gdk_rgba_parse (&color, val);
    if (valid)
	vte_set_color_func (vte, &color);
}

static void config_apply_colors (config_t c, VteTerminal* vte)
{
    GdkRGBA* palette = g_new0(GdkRGBA, 16);
    char key[32];
    for (unsigned i = 0; i < 16; i++)
    {
	g_snprintf (key, sizeof (key), "config.color_%d", i);
	const char *val = config_get_string (c, key, NULL);
	if (val)
	{
	    bool valid = gdk_rgba_parse(&palette[i], val);
	    if (valid) continue;
	}

	// calc default color
	palette[i].blue  = (((i & 5) ? 0xc000 : 0) + (i > 7 ? 0x3fff : 0)) / 65535.0;
	palette[i].green = (((i & 2) ? 0xc000 : 0) + (i > 7 ? 0x3fff : 0)) / 65535.0;
	palette[i].red	 = (((i & 1) ? 0xc000 : 0) + (i > 7 ? 0x3fff : 0)) / 65535.0;
	palette[i].alpha = 0;
    }
    vte_terminal_set_colors (vte, NULL, NULL, palette, 16);
}

static void context_apply (config_t c, VteTerminal *vte)
{
    //GtkWindow* window = GTK_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(vte)));

    // config
    vte_terminal_set_cursor_blink_mode (vte, match_cursor_blink_mode (
	config_get_string (c, "config.cursor_blink_mode", CURSOR_BLINK_MODE_SYSTEM)
	));
    vte_terminal_set_cjk_ambiguous_width (vte, match_cjk_width (
	config_get_string (c, "config.cjk_width", CJK_WIDTH_NARROW)
	));

    // font
    const char *font = config_get_string (c, "config.font", NULL);
    if (font)
    {
	PangoFontDescription* font_desc = pango_font_description_from_string (font);
	vte_terminal_set_font (vte, font_desc);
	pango_font_description_free (font_desc);
    }

    // colors
    config_apply_colors (c, vte);
    config_apply_color	(c, vte, vte_terminal_set_color_bold, "config.color_bold");
    config_apply_color	(c, vte, vte_terminal_set_color_background, "config.color_background");
    config_apply_color	(c, vte, vte_terminal_set_color_foreground, "config.color_foreground");
    config_apply_color	(c, vte, vte_terminal_set_color_cursor, "config.color_cursor");
    config_apply_color	(c, vte, vte_terminal_set_color_highlight, "config.color_highlight");
    config_apply_color	(c, vte, vte_terminal_set_color_highlight_foreground, "config.color_highlight_foreground");
    config_apply_color	(c, vte, vte_terminal_set_color_cursor_foreground, "config.color_cursor_foreground");
}

/*
 * command functions
 */
typedef void (*TymCommandFunc)(Context* context);

typedef struct {
  unsigned key;
  GdkModifierType mod;
  TymCommandFunc func;
} KeyPair;

/*
 * key accelerators
 */
static KeyPair default_key_pairs[] =
{
    { GDK_KEY_plus , GDK_CONTROL_MASK		      , command_increase_font_scale },
    { GDK_KEY_minus, GDK_CONTROL_MASK		      , command_decrease_font_scale },
    { GDK_KEY_equal, GDK_CONTROL_MASK		      , command_reset_font_scale    },
    { GDK_KEY_c    , GDK_CONTROL_MASK | GDK_SHIFT_MASK, command_copy_clipboard	    },
    { GDK_KEY_v    , GDK_CONTROL_MASK | GDK_SHIFT_MASK, command_paste_clipboard     },
    { GDK_KEY_r    , GDK_CONTROL_MASK | GDK_SHIFT_MASK, command_reload		    },
    { 0 	   , 0				      , NULL			    }
};

Context * context_init (view_t v, vterm_t vt, config_t cfg, void *extra)
{
    VteTerminal    *vte = (VteTerminal *) vt;
    Context    *context = g_malloc0(sizeof(Context));
    context->view   = v;
    context->vte    = vte;
    context->extra  = extra;
    context->config = cfg;
    context->keymap = keymap_init ();
    return context;
}

void context_close (Context *context)
{
    keymap_close (context->keymap);
    g_free (context);
}

void context_load (Context* context)
{
    if (context->loading)
    {
	dd("load exit for recursive loading");
	g_warning("tried to load config recursively");
	return;
    }

    context->loading = true;
    keymap_reset (context->keymap);
    context_apply(context->config, context->vte);
    context->loading = false;
    return;
}

static bool context_perform_default (Context* context, unsigned key, GdkModifierType mod)
{
    for (unsigned i = 0; default_key_pairs[i].func; i++)
    {
	if ((key == default_key_pairs[i].key) && !(~mod & default_key_pairs[i].mod))
	{
	    default_key_pairs[i].func(context);
	    return true;
	}
    }
    return false;
}

bool context_perform_keymap (Context* context, unsigned key, unsigned mod)
{
    if (config_get_use_default_keymap(context->config)
    &&	context_perform_default(context, key, mod))
	return true;
    return false;
}
