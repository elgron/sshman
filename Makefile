#
# simple makefile if you don't have 'doit'
#
SRCS = context.c main.c view.c
EXTRALIBS = $(shell pkg-config --libs vte-2.91)
EXTRAOPTS = $(shell pkg-config --cflags vte-2.91)

sshman: internal.h $(SRCS)
	gcc -O2 -Wall -std=gnu11 -D_GNU_SOURCE -I. $(EXTRAOPTS) -o $@ $(SRCS) $(EXTRALIBS)
