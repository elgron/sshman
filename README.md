# sshman

Simple multi tab ssh connection manager using gtk-3 and vte-2

# Key Bindings

* **CTRL +** Increase font size
* **CTRL -** Decrease font size
* **CTRL =** Reset font size
* **CTRL c** Copy
* **CTRL p** Paste
* **CTRL r** Reload
* **CTRL <-** Go to left tab
* **CTRL ->** Go to right tab

