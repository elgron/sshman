#ifndef _internal_h_
#define _internal_h_
/*
 * internal.h
 *
 * sshman api
 * (c) JCGV, junio del 2018
 *
 */
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#define VERSION     "0.1.0"

/*
 * debug support
 */
#ifdef DEBUG
#define dd( fmt, ... )	fprintf(stderr, "[%s:%u] " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#else
#define dd( ... )	((void)0)
#endif

/*
 * objects
 */
typedef struct config_t  * config_t;
typedef struct context_t * context_t;
typedef struct global_t  * global_t;
typedef struct host_t	 * host_t;
typedef struct view_t	 * view_t;
typedef struct vterm_t	 * vterm_t;

/*
 * global state
 */
struct global_t
{
    view_t	view;
    config_t	cfg;
    const char *config_file;
};

bool global_reload (global_t);

/*
 * config
 */
const char * config_get_string (config_t, const char *key, const char *def);
bool	     config_get_bool   (config_t, const char *key, bool def);
long	     config_get_int    (config_t, const char *key, long def);

const char * config_get_launcher (config_t);
bool	     config_get_use_default_keymap (config_t);

const char * config_begin (config_t);
const char * config_next  (config_t);
const char * config_host  (config_t);

struct host_t
{
    const char *name;
    const char *host;
    const char *user;
};

host_t config_find (config_t, const char *);

/*
 * view
 */
view_t view_begin (global_t, const char *title);
void   view_done  (view_t);
int    view_go	  (view_t);

/*
 * context
 */
context_t context_init	(view_t, vterm_t, config_t, void *);
void	  context_close (context_t);
void	  context_load	(context_t);
bool	  context_perform_keymap (context_t, unsigned key, unsigned mod);

vterm_t   context_vterm (context_t);
view_t	  context_view	(context_t);
void *	  context_extra (context_t);

#endif /* _internal_h_ */
