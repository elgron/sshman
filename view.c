/*
 * view.c
 *
 * sshman gui
 * (c) JCGV, junio del 2018
 *
 */
#include "internal.h"
#include <gtk/gtk.h>
#include <vte/vte.h>

/*
 * view
 */
struct view_t
{
    global_t	    g;
    GtkApplication  *app;
    GtkWindow	    *window;
    GtkWidget	    *treeview;
    GtkWidget	    *book;
    const char	    *title;
};

/*
 * tree model
 */
enum {SESSION_NAME_COLUMN = 0, NUM_COLUMNS};

static GtkTreeModel * create_model (config_t cfg)
{
    /* create tree store */
    GtkTreeStore *model = gtk_tree_store_new (NUM_COLUMNS, G_TYPE_STRING);

    /* add data to the tree store */
    for (const char *grp = config_begin (cfg); grp; grp = config_next (cfg))
    {
	GtkTreeIter iter;
	gtk_tree_store_append (model, &iter, NULL);
	gtk_tree_store_set (model, &iter, SESSION_NAME_COLUMN, grp, -1);

	/* add children */
	for (const char *host = config_host (cfg); host; host = config_host (cfg))
	{
	    GtkTreeIter child_iter;
	    gtk_tree_store_append (model, &child_iter, &iter);
	    gtk_tree_store_set (model, &child_iter, SESSION_NAME_COLUMN, host, -1);
	}
    }
    return GTK_TREE_MODEL (model);
}

/*
 * session launch
 */
static void on_child_exited (VteTerminal *vte, int status, void* user_data)
{
    GtkWidget *button = user_data;
    gtk_widget_set_sensitive (button, TRUE);
}

static bool on_key_press (GtkWidget *widget, GdkEventKey *event, void* user_data)
{
    context_t context = (context_t) user_data;
    unsigned mod = event->state & gtk_accelerator_get_default_mod_mask();
    unsigned key = gdk_keyval_to_lower(event->keyval);
    if (mod == GDK_CONTROL_MASK && (key == GDK_KEY_Left || key == GDK_KEY_Right))
    {
	view_t v = context_view (context);
	int   np = gtk_notebook_get_n_pages (GTK_NOTEBOOK (v->book));
	int    n = gtk_notebook_get_current_page (GTK_NOTEBOOK (v->book));
	if (key == GDK_KEY_Left && n > 0)
	    gtk_notebook_set_current_page (GTK_NOTEBOOK (v->book), n - 1);
	else if (key == GDK_KEY_Right && n + 1 < np)
	    gtk_notebook_set_current_page (GTK_NOTEBOOK (v->book), n + 1);
	return true;
    }
    return context_perform_keymap (context, key, mod);
}

static void sel_changed (VteTerminal *vte, gpointer data)
{
    if (vte_terminal_get_has_selection (vte))
	vte_terminal_copy_clipboard_format (vte, VTE_FORMAT_TEXT);
}

static int button_pressed (GtkWidget *widget, GdkEventButton *event, gpointer data)
{
    //printf ("button pressed %i\n", event->button); fflush (stdout);
    switch (event->button)
    {
	case 3: // right click
	    vte_terminal_paste_clipboard (VTE_TERMINAL (widget));
	    return TRUE;
	default:
	    break;
    }
    return FALSE;
}

static void close_tab (GtkWidget *button, context_t context)
{
    GtkWidget *w = context_extra (context);
    view_t     v = context_view (context);
    unsigned np  = gtk_notebook_get_n_pages (GTK_NOTEBOOK (v->book));
    for (unsigned i = 0; i < np; ++i)
    {
	GtkWidget *ww = gtk_notebook_get_nth_page (GTK_NOTEBOOK (v->book), i);
	if (ww == w)
	{
	    gtk_notebook_remove_page (GTK_NOTEBOOK (v->book), i);
	    break;
	}
    }
    context_close (context);
}

static void on_spawn (VteTerminal *vte, GPid pid, GError *error, void* user_data)
{
    if (error)
	g_warning("vte spwan failed for: %s", error->message);
    else
    {
	gtk_widget_grab_focus (GTK_WIDGET (vte));
	g_signal_connect (GTK_WIDGET (vte), "button-press-event", G_CALLBACK(button_pressed), user_data);
	g_signal_connect (GTK_WIDGET (vte), "selection-changed", G_CALLBACK(sel_changed), user_data);
    }
}

static void tree_notify (GtkTreeView *t, GtkTreePath *path, GtkTreeViewColumn *c, gpointer userdata)
{
    view_t	      v = userdata;
    GtkTreeModel *model = gtk_tree_view_get_model (t);
    GtkTreeIter   iter;
    if (gtk_tree_model_get_iter(model, &iter, path))
    {
	// get selection host
	gchar *name;
	gtk_tree_model_get (model, &iter, SESSION_NAME_COLUMN, &name, -1);
	host_t h = config_find (v->g->cfg, name);
	//printf ("notify [%s]\n", name); fflush (stdout);
	g_free (name);

	// found ?
	if (h)
	{
	    // create terminal
	    VteTerminal *vte = VTE_TERMINAL (vte_terminal_new ());
	    vte_terminal_set_rewrap_on_resize (vte, true);
	    vte_terminal_set_scrollback_lines (vte, -1);

	    // create header
	    GtkWidget *box    = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 4);
	    GtkWidget *button = gtk_button_new_from_icon_name ("edit-delete-symbolic", GTK_ICON_SIZE_BUTTON);
	    gtk_box_pack_start (GTK_BOX (box), button, FALSE, FALSE, 0);
	    gtk_box_pack_start (GTK_BOX (box), gtk_label_new (h->name), TRUE, TRUE, 0);
	    gtk_widget_set_sensitive (button, FALSE);
	    gtk_widget_show_all (GTK_WIDGET (box));

	    // scroll
	    GtkWidget *sw = gtk_scrolled_window_new (NULL, NULL);
	    gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_ETCHED_OUT);
	    gtk_scrolled_window_set_policy	(GTK_SCROLLED_WINDOW (sw), GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);
	    gtk_scrolled_window_set_placement	(GTK_SCROLLED_WINDOW (sw), GTK_CORNER_TOP_RIGHT);
	    gtk_scrolled_window_set_overlay_scrolling (GTK_SCROLLED_WINDOW (sw), FALSE);
	    gtk_container_add (GTK_CONTAINER (sw), GTK_WIDGET (vte));
	    gtk_widget_show_all (GTK_WIDGET (sw));

	    // add to notebook
	    int np = gtk_notebook_append_page (GTK_NOTEBOOK (v->book), sw, box);
	    gtk_notebook_set_tab_reorderable (GTK_NOTEBOOK (v->book), sw, TRUE);

	    // create context
	    context_t context = context_init (v, (vterm_t) vte, v->g->cfg, sw);
	    context_load (context);
	    g_signal_connect (G_OBJECT (vte), "child-exited", G_CALLBACK (on_child_exited), button);
	    g_signal_connect (G_OBJECT (vte), "key-press-event", G_CALLBACK (on_key_press), context);
	    g_signal_connect (button, "clicked", G_CALLBACK (close_tab), context);

	    // launch terminal
	    char* argv[] = {
		(char *) config_get_launcher (v->g->cfg), (char *) h->host, (char *) h->user, NULL
		};
	    char** env = g_get_environ();
	    vte_terminal_spawn_async (
		vte,		     // terminal
		VTE_PTY_DEFAULT,     // pty flag
		NULL,		     // working directory
		argv,		     // argv
		env,		     // envv
		G_SPAWN_SEARCH_PATH, // spawn_flags
		NULL,		     // child_setup
		NULL,		     // child_setup_data
		NULL,		     // child_setup_data_destroy
		1000,		     // timeout
		NULL,		     // cancel callback
		on_spawn,	     // callback
		NULL		     // user_data
	    );
	    g_strfreev(env);

	    // show
	    gtk_notebook_set_current_page (GTK_NOTEBOOK (v->book), np);
	}
    }
}

static void add_columns (GtkTreeView *treeview, view_t v)
{
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new ();
    g_object_set (renderer, "xalign", 0.0, NULL);

    /*int col_offset =*/ gtk_tree_view_insert_column_with_attributes (
	GTK_TREE_VIEW (treeview), -1, "SSH Sessions", renderer, "text", SESSION_NAME_COLUMN, NULL
	);
    //GtkTreeViewColumn *column = gtk_tree_view_get_column (GTK_TREE_VIEW (treeview), col_offset - 1);
    //gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
    g_signal_connect (treeview, "row-activated", G_CALLBACK (tree_notify), v);
}

/*
 * reload
 */
static void reload (GtkWidget *button, view_t v)
{
    global_t g = v->g;
    if (global_reload (g))
    {
	GtkTreeModel *model = create_model (g->cfg);
	gtk_tree_view_set_model (GTK_TREE_VIEW (v->treeview), model);
	g_object_unref (model);
    }
}

/*
 * done & go
 */
void view_done (view_t v)
{
}

int view_go (view_t v)
{
    int rc = g_application_run (G_APPLICATION (v->app), 0, NULL);
    g_object_unref (v->app);
    return rc;
}

/*
 * setup
 */
static void on_response (GtkDialog *d, gint response_id, gpointer user_data)
{
    bool *pb = (bool *) user_data;
    switch (response_id)
    {
	case GTK_RESPONSE_OK: *pb = true;  break;
	default:	      *pb = false; break;
    }
    gtk_widget_destroy (GTK_WIDGET (d));
}

static gboolean delete_event (GtkWidget *w, GdkEvent *event, void *user_data)
{
    // want to exit ?
    view_t	    v = (view_t) user_data;
    unsigned	   np = gtk_notebook_get_n_pages (GTK_NOTEBOOK (v->book));
    bool want_to_exit = true;
    if (np > 0)
    {
	GtkWidget *d = gtk_message_dialog_new (v->window,
	    GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, GTK_BUTTONS_OK_CANCEL,
	    "There are open ssh sessions. Do you really want to quit ?."
	    );
	gtk_widget_show_all (d);
	g_signal_connect (GTK_DIALOG (d), "response", G_CALLBACK (on_response), &want_to_exit);
	gtk_dialog_run (GTK_DIALOG (d));
    }

    // done
    return want_to_exit ? FALSE : TRUE;
}

static void on_activate(GtkApplication* app, void* user_data)
{
    // get view
    view_t v = user_data;

    // another instance ?
    GList* list = gtk_application_get_windows(app);
    if (list) {
	gtk_window_present(GTK_WINDOW(list->data));
	return;
    }

    // create main window
    v->window = GTK_WINDOW (gtk_application_window_new (app));
    gtk_window_set_icon_name (v->window, "terminal");
    gtk_window_set_title (v->window, v->title);
    gtk_container_set_border_width (GTK_CONTAINER (v->window), 8);
    gtk_window_set_default_size (GTK_WINDOW(v->window),
	config_get_int (v->g->cfg, "config.width", 1024),
	config_get_int (v->g->cfg, "config.height", 768)
	);

    // separation
    GtkWidget *vpaned = gtk_paned_new (GTK_ORIENTATION_HORIZONTAL);
    gtk_container_set_border_width (GTK_CONTAINER(vpaned), 5);
    gtk_container_add (GTK_CONTAINER (v->window), vpaned);

    // left box
    GtkWidget *box    = gtk_box_new (GTK_ORIENTATION_VERTICAL, 4);
    GtkWidget *button = gtk_button_new_with_label ("Reload");
    gtk_box_pack_start (GTK_BOX (box), button, FALSE, FALSE, 0);
    gtk_paned_add1 (GTK_PANED (vpaned), box);
    g_signal_connect (button, "clicked", G_CALLBACK (reload), v);

    // scrolled area
    GtkWidget *sw = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_ETCHED_IN);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_widget_set_size_request (sw, config_get_int (v->g->cfg, "config.left-size", 200), 256);
    gtk_box_pack_start (GTK_BOX (box), sw, TRUE, TRUE, 0);

    // model & treeview
    GtkTreeModel *model = create_model (v->g->cfg);
    v->treeview = gtk_tree_view_new_with_model (model);
    g_object_unref (model);
    gtk_tree_selection_set_mode (gtk_tree_view_get_selection (GTK_TREE_VIEW (v->treeview)), GTK_SELECTION_MULTIPLE);
    add_columns (GTK_TREE_VIEW (v->treeview), v);
    gtk_container_add (GTK_CONTAINER (sw), v->treeview);
    g_signal_connect (v->treeview, "realize", G_CALLBACK (gtk_tree_view_collapse_all), NULL);

    // notebook
    v->book = gtk_notebook_new ();
    gtk_notebook_set_scrollable (GTK_NOTEBOOK (v->book), TRUE);
    gtk_paned_add2 (GTK_PANED (vpaned), GTK_WIDGET (v->book));

#if 0
    // default background
    GdkRGBA color;
    gdk_rgba_parse (&color, "rgb(65,173,65)");
    gtk_widget_override_background_color (v->book, GTK_STATE_NORMAL, &color);
#endif

    // center
    gtk_window_set_position(GTK_WINDOW (v->window), GTK_WIN_POS_CENTER_ALWAYS);

    // exit check
    g_signal_connect (G_OBJECT (v->window), "delete-event", G_CALLBACK (delete_event), v);

    // set icon
    GdkPixbuf *icon = gdk_pixbuf_new_from_file_at_size (
	config_get_string (v->g->cfg, "config.icon", "sshman.svg"),
	64, 64, NULL
	);
    if (icon) gtk_window_set_icon (GTK_WINDOW (v->window), icon);

    // done
    gtk_widget_show_all (GTK_WIDGET (v->window));
    gtk_widget_show (GTK_WIDGET (v->window));
}

/*
 * begin
 */
view_t view_begin (global_t g, const char *title)
{
    // create object
    view_t v = calloc (1, sizeof (*v));
    v->g     = g;
    v->title = title;

    // create app
    v->app = gtk_application_new ("org.elgron.sshman", G_APPLICATION_NON_UNIQUE);
    g_signal_connect (v->app, "activate", G_CALLBACK (on_activate), v);

    // done
    return v;
}
