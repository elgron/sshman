let cc	   = egcc
    cflags = -Wall -O1 -g -std=gnu11 -D_GNU_SOURCE -I. @shell pkg-config --cflags vte-2.91@
    libs   = @shell pkg-config --libs vte-2.91@

rule cc
  command = {cc} {cflags} -MMD -MF - -c -o {out} {in}
  deps	  = cc
  print   = cc {in}

rule link
  command = {cc} -g -o {out} {in} {libs}
  print   = link {out}

@exe ! sshman main.c view.c context.c@
