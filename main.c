/*
 * main.c
 *
 * sshman main logic
 * (c) JCGV, junio del 2018
 *
 */
#include "internal.h"
#include <stdio.h>

/*
 * config
 */
#define CFG_HASH    64

typedef struct node_t  * node_t;
typedef struct group_t * group_t;

struct config_t
{
    unsigned	iter, host;
    unsigned	ng;
    group_t	vg;
    node_t	v[CFG_HASH];
};

struct node_t
{
    node_t	next;
    const char *key;
    const char *val;
};

struct group_t
{
    const char *name;
    unsigned	nh;
    host_t	vh;
};

static int group_cmp (const void *a, const void *b)
{
    group_t g0 = (group_t) a;
    group_t g1 = (group_t) b;
    return strcmp (g0->name, g1->name);
}

static int host_cmp (const void *a, const void *b)
{
    host_t h0 = (host_t) a;
    host_t h1 = (host_t) b;
    return strcmp (h0->name, h1->name);
}

static unsigned hash (const char *s)
{
#define BITS_IN_UNSIGNED	(sizeof (unsigned) == 4 ? 32 : 16)
#define SEVENTY_FIVE_PERCENT	((BITS_IN_UNSIGNED >> 2) * 3)
#define TWELVE_PERCENT		(BITS_IN_UNSIGNED >> 3)
#define HIGH_BITS		(~((unsigned) (~0) >> TWELVE_PERCENT))

    const unsigned char *ide = (const unsigned char *) s;
    unsigned g, h = 0;
    while (*ide)
    {
	h = (h << TWELVE_PERCENT) + *ide++;
	g = h & HIGH_BITS;
	if (g)
	    h = (h ^ (g >> SEVENTY_FIVE_PERCENT)) & ~HIGH_BITS;
    }
    return h;

#undef BITS_IN_UNSIGNED
#undef SEVENTY_FIVE_PERCENT
#undef TWELVE_PERCENT
#undef HIGH_BITS
}

static const char * chop (const char *s)
{
    // begin
    const uint8_t *b = (const uint8_t *) s;
    while (*b <= ' ')
	++b;
    if (*b == '\'') ++b;

    // end
    const uint8_t *e = b + strlen ((const char *) b);
    while (e > b && e[-1] <= ' ')
	--e;
    if (e > b && e[-1] == '\'') --e;

    // alloc
    int len = e - b;
    char *r = malloc (len + 1);
    memcpy (r, b, len);
    r[len] = 0;
    return r;
}

static void config_add (config_t c, const char *key, const char *val)
{
    // create node
    node_t n = malloc (sizeof (*n));
    n->key   = chop (key);
    n->val   = chop (val);

    // add
    unsigned h = hash (n->key) % CFG_HASH;
    n->next    = c->v[h];
    c->v[h]    = n;
    //printf ("update [%s] with [%s] at %u\n", n->key, n->val, h);
}

static config_t config_parse (const char *path)
{
    // create config
    config_t c = calloc (1, sizeof (*c));

    // set defaults
    config_add (c, "config.shell", "/bin/sh");

    // parse config
    FILE *f = fopen (path, "r");
    if (!f)
    {
	fprintf (stderr, "sshman: can not open file [%s]\n", path);
	free	(c);
	c = NULL;
    }
    else
    {
	// parse state
	group_t g = NULL;
	host_t	h = NULL;

	// parse lines
	char iobuf[512];
	int  lineno = 0;
	memset (iobuf, 0, sizeof (iobuf));
	while (c && fgets (iobuf, sizeof (iobuf), f))
	{
	    // lineno
	    ++lineno;

	    // parse
	    uint8_t *p = (uint8_t *) iobuf;
	    while (*p && *p <= ' ')
		++p;
	    switch (*p)
	    {
		case 0:
		case '#':
		    // ignore empty lines and comments
		    break;
		case '@':
		    {
			unsigned chunk = 8;
			if (c->ng % chunk == 0)
			    c->vg = realloc (c->vg, (c->ng + chunk) * sizeof (struct group_t));

			g	= &c->vg[c->ng++];
			g->name = chop ((const char *) p + 1);
			g->nh	= 0;
			g->vh	= NULL;
		    }
		    break;
		case '+':
		    if (!g)
		    {
			fflush	(stdout);
			fprintf (stderr, "sshman: host without group at line %i in [%s]:\n%s\n", lineno, path, iobuf);
			free	(c); c = NULL;
		    }
		    else
		    {
			unsigned chunk = 8;
			if (g->nh % chunk == 0)
			    g->vh = realloc (g->vh, (g->nh + chunk) * sizeof (struct host_t));

			h	= &g->vh[g->nh++];
			h->name = chop ((const char *) p + 1);
			h->host = NULL;
			h->user = NULL;
		    }
		    break;
		case '=':
		case '?':
		    if (!h)
		    {
			fflush	(stdout);
			fprintf (stderr, "sshman: host attr without host at line %i in [%s]:\n%s\n", lineno, path, iobuf);
			free	(c); c = NULL;
		    }
		    else
		    {
			const char *v = chop ((const char *) p + 1);
			switch (*p)
			{
			    case '=': h->host = v; break;
			    case '?': h->user = v; break;
			}
		    }
		    break;
		default:
		    {
			// get key
			uint8_t *key = p;
			while (*p && *p != '=')
			    ++p;
			if (*p != '=')
			{
			    fflush  (stdout);
			    fprintf (stderr, "sshman: invalid config line %i in [%s]:\n%s\n", lineno, path, iobuf);
			    free    (c); c = NULL;
			}
			*p++ = 0;

			// get value
			uint8_t *val = p;

			// update
			config_add (c, (const char *) key, (const char *) val);
		    }
		    break;
	    }

	    // next
	    memset (iobuf, 0, sizeof (iobuf));
	}
	fclose (f);
    }

    // sort
    if (c)
    {
	// sort groups
	qsort (c->vg, c->ng, sizeof (struct group_t), group_cmp);

	// sort hosts
	for (unsigned i = 0; i < c->ng; ++i)
	{
	    group_t g = &c->vg[i];
	    qsort (g->vh, g->nh, sizeof (struct host_t), host_cmp);
	}
    }

    // done
    return c;
}

/*
 * query config
 */
static node_t config_node (config_t c, const char *key)
{
    unsigned h = hash (key) % CFG_HASH;
    for (node_t n = c->v[h]; n; n = n->next)
	if (!strcmp (n->key, key))
	    return n;
    return NULL;
}

const char * config_get_string (config_t c, const char *key, const char *def)
{
    node_t n = config_node (c, key);
    return n ? n->val : def;
}

bool config_get_bool (config_t c, const char *key, bool def)
{
    node_t n = config_node (c, key);
    return n ? (!strcmp (n->val, "true") ? true : false) : def;
}

long config_get_int (config_t c, const char *key, long def)
{
    node_t n = config_node (c, key);
    return n ? atol (n->val) : def;
}

const char * config_get_launcher (config_t c)
{
    return config_get_string (c, "config.launcher", "/bin/echo");
}

bool config_get_use_default_keymap (config_t c)
{
    return config_get_bool (c, "config.use_default_keymap", true);
}

/*
 * connections iteration
 */
const char * config_begin (config_t c)
{
    c->iter = -1;
    return config_next (c);
}

const char * config_next (config_t c)
{
    c->host = 0;
    ++c->iter;
    return c->iter < c->ng ? c->vg[c->iter].name : NULL;
}

const char * config_host (config_t c)
{
    group_t g = &c->vg[c->iter];
    return c->host < g->nh ? g->vh[c->host++].name : NULL;
}

/*
 * search host
 */
host_t config_find (config_t c, const char *id)
{
    for (unsigned i = 0; i < c->ng; ++i)
    {
	group_t g = &c->vg[i];
	for (unsigned n = 0; n < g->nh; ++n)
	{
	    host_t h = &g->vh[n];
	    if (!strcmp (id, h->name))
		return h;
	}
    }
    return NULL;
}

/*
 * reload
 */
bool global_reload (global_t g)
{
    config_t c = config_parse (g->config_file);
    if (c)
    {
	//config_destroy (g->cfg);
	g->cfg = c;
	return true;
    }
    return false;
}

/*
 * entry point
 */
int main (int argc, char **argv)
{
    // create global state
    global_t g = calloc (1, sizeof (*g));
    asprintf ((char **) &g->config_file, "%s/settings/sshman.cfg", getenv ("HOME"));

    // create config
    g->cfg = config_parse (g->config_file);
    if (!g->cfg)
	return EXIT_FAILURE;

    // exit code
    int rc = EXIT_FAILURE;

    // create view
    g->view = view_begin (g, "sshman " VERSION);
    if (g->view)
    {
	// loop
	rc = view_go (g->view);

	// cleanup
	view_done (g->view);
    }

    // done
    return rc;
}
